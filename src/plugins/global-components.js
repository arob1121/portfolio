import app from './main-app.js'

import BasePage from '@/components/layout/BasePage.vue'
import BaseHeader from '@/components/layout/BaseHeader.vue'
import BaseBody from '@/components/layout/BaseBody.vue'
import BaseFooter from '@/components/layout/BaseFooter.vue'
import BaseSection from '@/components/layout/BaseSection.vue'
import BaseAccordion from '@/components/layout/BaseAccordion.vue'
import BaseCard from '@/components/layout/BaseCard.vue'
import BaseCardSection from '@/components/layout/BaseCardSection.vue'
import BaseGrid from '@/components/layout/BaseGrid.vue'
import BaseGridContainer from '@/components/layout/BaseGridContainer.vue'
import BaseSidebar from '@/components/layout/BaseSidebar.vue'
import BaseStack from '@/components/layout/BaseStack.vue'
import BaseNavigation from '@/components/layout/BaseNavigation.vue'
import BaseNavigationGroup from '@/components/layout/BaseNavigationGroup.vue'
import BaseTable from '@/components/layout/BaseTable.vue'
import BaseTableRow from '@/components/layout/BaseTableRow.vue'
import BaseTableCell from '@/components/layout/BaseTableCell.vue'

import BaseParagraph from '@/components/foundation/typography/BaseParagraph.vue'
import BaseHeading from '@/components/foundation/typography/BaseHeading.vue'
import BaseQuote from '@/components/foundation/typography/BaseQuote.vue'
import BaseDivider from '@/components/foundation/BaseDivider.vue'

import BaseAvatar from '@/components/element/BaseAvatar.vue'
import BaseButton from '@/components/element/BaseButton.vue'

import BaseForm from '@/components/form/BaseForm.vue'
import BaseFormContainer from '@/components/form/BaseFormContainer.vue'
import BaseInput from '@/components/form/BaseInput.vue'
import BaseTextarea from '@/components/form/BaseTextarea.vue'

import BaseImage from '@/components/media/BaseImage.vue'
import BaseLogo from '@/components/media/BaseLogo.vue'
import BaseSlider from '@/components/media/BaseSlider.vue'
import BaseSliderContainer from '@/components/media/BaseSliderContainer.vue'
import BaseIcon from '@/components/media/BaseIcon.vue'

import BaseLink from '@/components/navigation/BaseLink.vue'

import BaseDropdown from '@/components/overlay/BaseDropdown.vue'
import BaseDrawer from '@/components/overlay/BaseDrawer.vue'
import BaseModal from '@/components/overlay/BaseModal.vue'

// Anna Robbins Project Specific
import BaseColor from '@/components/anna-robbins/BaseColor.vue'

// Design System Project Specific
import BaseTheme from '@/components/design-system/BaseTheme.vue'
import BaseStage from '@/components/design-system/BaseStage.vue'

export default app
  .component('Page', BasePage)
  .component('Header', BaseHeader)
  .component('Body', BaseBody)
  .component('Footer', BaseFooter)
  .component('Section', BaseSection)
  .component('Accordion', BaseAccordion)
  .component('Card', BaseCard)
  .component('CardSection', BaseCardSection)
  .component('Grid', BaseGrid)
  .component('GridContainer', BaseGridContainer)
  .component('Sidebar', BaseSidebar)
  .component('Stack', BaseStack)
  .component('Navigation', BaseNavigation)
  .component('NavigationGroup', BaseNavigationGroup)
  .component('Table', BaseTable)
  .component('TableRow', BaseTableRow)
  .component('TableCell', BaseTableCell)

  .component('Paragraph', BaseParagraph)
  .component('Heading', BaseHeading)
  .component('Quote', BaseQuote)
  .component('Divider', BaseDivider)

  .component('Avatar', BaseAvatar)
  .component('Button', BaseButton)

  .component('Form', BaseForm)
  .component('FormContainer', BaseFormContainer)
  .component('Input', BaseInput)
  .component('Textarea', BaseTextarea)

  .component('Images', BaseImage)
  .component('Logo', BaseLogo)
  .component('Slider', BaseSlider)
  .component('SliderContainer', BaseSliderContainer)
  .component('Icon', BaseIcon)

  .component('Link', BaseLink)

  .component('Dropdown', BaseDropdown)
  .component('Drawer', BaseDrawer)
  .component('Modal', BaseModal)

  .component('Color', BaseColor)

  .component('Theme', BaseTheme)
  .component('Stage', BaseStage)
