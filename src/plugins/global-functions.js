import app from './main-app.js'

const clickOutsideDirective = {
  mounted(el, binding) {
    el.clickOutsideEvent = function (event) {
      if (!(el == event.target || el.contains(event.target))) {
        binding.value() // Call the handler function
      }
    }
    document.addEventListener('click', el.clickOutsideEvent)
  },
  unmounted(el) {
    document.removeEventListener('click', el.clickOutsideEvent)
  },
}

app.directive('click-outside', clickOutsideDirective)

app.config.globalProperties.$stageTheme = (id) => {
  id = localStorage.getItem('theme')
  var stageElement = document.querySelectorAll('.stage')
  var themes = ['theme-ds', 'theme-ar', 'theme-sd', 'theme-wm', 'theme-dl', 'theme-ed', 'theme-hv']

  for (var i = 0; i < stageElement.length; i++) {
    for (var j = 0; j < themes.length; j++) {
      if (id === themes[j]) {
        stageElement[i].setAttribute('theme', themes[j])
        localStorage.setItem('theme', themes[j])
      }
    }
  }
  var themeElement = document.querySelectorAll('.theme')
  for (var a = 0; a < themeElement.length; a++) {
    for (var b = 0; b < themes.length; b++) {
      if (id === themes[b]) {
        themeElement[a].setAttribute('stage', themes[b])
      }
    }
  }
}
app.config.globalProperties.$classNames = (item) => {
  let id = item.id
  let div = document.getElementById(id)
  let name = item.name
  let prop = item.prop
  let j = item.value

  if (j !== undefined) {
    if (prop === 'visibility') {
      if (j.indexOf(' ') >= 0) {
        var s1 = j.split(' ')[0]
        var s2 = j.split(' ')[1]
        var s3 = j.split(' ')[2]
        var s4 = j.split(' ')[3]

        var s5 = s1.substring(0, s1.indexOf('@'))
        var s6 = s1.substring(s1.indexOf('@') + 1)

        div.classList.add(name + '--' + s5 + '-' + s6)

        if (s2) {
          var s7 = s2.substring(0, s2.indexOf('@'))
          var s8 = s2.substring(s2.indexOf('@') + 1)
          div.classList.add(name + '--' + s7 + '-' + s8)
        }
        if (s3) {
          var s9 = s3.substring(0, s3.indexOf('@'))
          var s0 = s3.substring(s3.indexOf('@') + 1)
          div.classList.add(name + '--' + s9 + '-' + s0)
        }
        if (s4) {
          var s10 = s4.substring(0, s4.indexOf('@'))
          var s11 = s4.substring(s4.indexOf('@') + 1)
          div.classList.add(name + '--' + s10 + '-' + s11)
        }
      } else {
        var a5 = j.substring(0, j.indexOf('@'))
        var a6 = j.substring(j.indexOf('@') + 1)
        div.classList.add(name + '--' + a5 + '-' + a6)
      }
    } else {
      if (j.indexOf(' ') >= 0) {
        var c1 = j.split(' ')[0]
        var c2 = j.split(' ')[1]
        var c3 = j.split(' ')[2]
        var c4 = j.split(' ')[3]

        var c5 = c2.substring(0, c2.indexOf('@'))
        var c6 = c2.substring(c2.indexOf('@') + 1)

        div.classList.add(name + '--' + prop + '-' + c1)
        div.classList.add(name + '--' + prop + '-' + c6 + '-' + c5)

        if (c3) {
          var c7 = c3.substring(0, c3.indexOf('@'))
          var c8 = c3.substring(c3.indexOf('@') + 1)
          div.classList.add(name + '--' + prop + '-' + c8 + '-' + c7)
        }
        if (c4) {
          var c9 = c4.substring(0, c4.indexOf('@'))
          var c0 = c4.substring(c4.indexOf('@') + 1)
          div.classList.add(name + '--' + prop + '-' + c0 + '-' + c9)
        }
      }
    }
  }
}
