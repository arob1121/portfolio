import app from './plugins/main-app.js'
import './plugins/global-components.js'
import './plugins/global-functions.js'
import router from './router/index.js'

app.use(router)
app.mount('#app')
